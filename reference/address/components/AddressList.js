import React from 'react';
import PropTypes from 'prop-types';
import AddressCard from './AddressCard';
import { Row, Col, Container } from 'reactstrap';
import { AddressStore, list, AddressActions } from '../AddressStore';
import { connect } from 'react-redux';
import { AddNew } from '../../../components';
const _ = require('lodash');
const mapStateToProps = state => {
    return { ...state.Address };
};
const propTypes = {
    setView: PropTypes.func,
    view: PropTypes.string,
    addresses: PropTypes.array.isRequired,
    primary: PropTypes.number,
};
class AddressList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static getDerivedStateFromProps() {
        window.setAccountNav();
        return null;
    }
    componentDidMount() {
        if (_.isEmpty(this.props.addresses)) {
            AddressStore.dispatch(list());
        }
        window.setAccountNav();
    }
    render() {
        const { view, setView, primary } = this.props;
        const addresses = _.sortBy(this.props.addresses, function(address) {
            if (address && address.id === primary) {
                return 1;
            }
        });
        if ('list' !== view) return false;

        return (
            <React.Fragment>
                {addresses && addresses.length > 0 && (
                    <Container>
                        <h1>
                            Address List
                            <br />
                            <AddNew onClick={() => setView('form')} />
                        </h1>

                        <Row>
                            {addresses &&
                                addresses.map((address, key) => {
                                    return (
                                        <Col key={key} mx="12" md="4">
                                            <AddressCard
                                                index={key}
                                                {...address}
                                            />
                                        </Col>
                                    );
                                })}
                        </Row>
                    </Container>
                )}

                {addresses && 0 === addresses.length && (
                    <center className="no-address">
                        <h3>
                            You have not added laundry delivery address. <br />
                            <a onClick={() => setView('form')}>
                                Click here to add address.
                            </a>
                        </h3>
                    </center>
                )}
            </React.Fragment>
        );
    }
}
AddressList.propTypes = propTypes;
export default connect(mapStateToProps, { ...AddressActions })(AddressList);
