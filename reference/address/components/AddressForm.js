import React from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import { Row, Col } from 'reactstrap';
import AddressService from '../AddressService';
import { FormTextInput } from '../../../components/FormInputs';
import {
    AddressStore,
    store,
    update,
    AddressActions,
    list,
} from '../AddressStore';
import { connect } from 'react-redux';
import { formValidation } from '../AddressSchema';
import { ViewList } from '../../../components';
const _ = require('lodash');
const mapStateToProps = state => {
    return { ...state.Address };
};
const propTypes = {
    address: PropTypes.object,
    addresses: PropTypes.array,
    setView: PropTypes.func,
    view: PropTypes.string,
};

class AddressForm extends React.Component {
    constructor(props) {
        super(props);
        const { initialValues, validations } = formValidation();
        this.state = {
            initialValues,
            validations,
            view: 'Submit',
        };
        this.addressService = new AddressService();
    }

    static getDerivedStateFromProps(props) {
        let state = {};
        if (false === _.isEmpty(props.addresses)) {
            state.addresses = props.addresses;
        }
        if (false === _.isEmpty(props.address)) {
            const { initialValues } = formValidation(props.address);
            state.initialValues = initialValues;
        } else {
            const { initialValues, validations } = formValidation();
            state.initialValues = initialValues;
            state.validations = validations;
        }
        return state;
    }

    onSubmit = async values => {
        if (false === _.isEmpty(values) && values.id) {
            AddressStore.dispatch(update(values));
        } else {
            AddressStore.dispatch(store(values));
        }
    };

    componentDidMount() {
        let { address } = this.state;
        if (false === _.isEmpty(address) && address.id) {
            const { initialValues } = formValidation(address);
            this.setState({ initialValues });
        }

        if (_.isEmpty(this.props.addresses)) {
            AddressStore.dispatch(list());
        }
        window.setAccountNav();
    }

    formAndInputs = props => {
        const { handleSubmit, errors, touched } = props;
        return (
            <Form onSubmit={handleSubmit}>
                <input type="hidden" id="id" name="id" />
                <h1>Address Details</h1>
                <Row>
                    <Col xs="12" md="4">
                        <FormTextInput
                            id="address_name"
                            name="address_name"
                            placeholder="Address Name (Home , Office)"
                            errors={errors}
                            touched={touched}
                        />
                    </Col>
                    <Col xs="12" md="8">
                        <FormTextInput
                            type="text"
                            id="address_line_1"
                            name="address_line_1"
                            placeholder="Address Line 1 (Home/Flat Street No. etc)"
                            errors={errors}
                            touched={touched}
                        />
                    </Col>
                    <Col xs="12" md="8">
                        <FormTextInput
                            type="text"
                            id="address_line_2"
                            name="address_line_2"
                            placeholder="Address Line 2 (Landmark)"
                            errors={errors}
                            touched={touched}
                        />
                    </Col>
                    <Col xs="12" md="4">
                        <FormTextInput
                            type="text"
                            id="postal_code"
                            name="postal_code"
                            placeholder="Postal Code"
                            errors={errors}
                            touched={touched}
                        />
                    </Col>
                </Row>
                <center>
                    <button type="submit" className="button">
                        {this.state.view}
                    </button>
                </center>
            </Form>
        );
    };

    render() {
        if ('form' !== this.props.view) return false;
        const FormAndInputs = this.formAndInputs;
        return (
            <Formik
                initialValues={this.state.initialValues}
                onSubmit={this.onSubmit}
                validationSchema={this.state.validations}
                validateOnBlur={false}
                validateOnChange={false}
                enableReinitialize={true}
            >
                {props => {
                    return (
                        <React.Fragment>
                            <ViewList
                                onClick={() => this.props.setView('list')}
                            />
                            <FormAndInputs {...props} />
                        </React.Fragment>
                    );
                }}
            </Formik>
        );
    }
}
AddressForm.propTypes = propTypes;
export default connect(mapStateToProps, { ...AddressActions })(AddressForm);
