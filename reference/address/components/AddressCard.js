import React from 'react';
import PropTypes from 'prop-types';
const propTypes = {
    index: PropTypes.number,
    id: PropTypes.any,
    address_name: PropTypes.string,
    receiver_name: PropTypes.string,
    address_line_1: PropTypes.string,
    address_line_2: PropTypes.string,
    postal_code: PropTypes.string,
    phone: PropTypes.string,
    edit: PropTypes.func.isRequired,
    primary: PropTypes.number,
};
import {
    AddressStore,
    remove,
    setPrimaryAddress,
    AddressActions,
} from '../AddressStore';
import { connect } from 'react-redux';
const mapStateToProps = state => {
    return { ...state.Address };
};
class AddressCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const {
            id,
            address_name,
            address_line_1,
            address_line_2,
            postal_code,
            primary,
        } = this.props;
        return (
            <div className="contact-box center-version">
                <a href="#">
                    <h6 className="m-b-xs">
                        <strong>{address_name}</strong>
                    </h6>

                    <address className="m-t-md">
                        {/* <strong>Twitter, Inc.</strong> */}
                        {address_line_1}
                        <br />
                        {address_line_2} - {postal_code}
                    </address>
                </a>
                <div className="contact-box-footer">
                    <div className="m-t-xs btn-group">
                        <div className="col-md-4 col-sm-4 col-xs-4 icon">
                            <center className="pull-left">
                                <a className="btn btn-xs btn-white">
                                    <i
                                        onClick={() =>
                                            AddressStore.dispatch(
                                                setPrimaryAddress(this.props.id)
                                            )
                                        }
                                        className={
                                            primary === id
                                                ? 'fa fa-star'
                                                : 'fa fa-star-o'
                                        }
                                    />
                                </a>
                            </center>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-4 icon">
                            <center>
                                <a
                                    onClick={() =>
                                        this.props.edit({
                                            id,
                                            address_name,
                                            address_line_1,
                                            address_line_2,
                                            postal_code,
                                        })
                                    }
                                    className="btn btn-xs btn-white"
                                >
                                    <i className="fa fa-edit" />
                                </a>
                            </center>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-4 icon">
                            <center className="pull-left">
                                <a
                                    onClick={() => {
                                        const { index, id } = this.props;
                                        AddressStore.dispatch(
                                            remove({ index, id })
                                        );
                                    }}
                                    className="btn btn-xs btn-red"
                                >
                                    <i className="fa fa-trash" />
                                </a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
AddressCard.propTypes = propTypes;
export default connect(mapStateToProps, { ...AddressActions })(AddressCard);
