import {
    createAsyncThunk,
    configureStore,
    getDefaultMiddleware,
    createSlice,
} from '@reduxjs/toolkit';
import { AddressMiddleware } from './AddressMiddleware';
const middleware = [...getDefaultMiddleware(), AddressMiddleware];
import AddressService from './AddressService';
const addressService = new AddressService();
export const store = createAsyncThunk('store', addressService.store);
export const list = createAsyncThunk('list', addressService.list);
export const update = createAsyncThunk('update', addressService.update);
export const remove = createAsyncThunk('delete', addressService.remove);
export const setPrimaryAddress = createAsyncThunk(
    'setprimaryaddress',
    addressService.setPrimaryAddress
);
//const _ = require('lodash');
const initialState = {
    primary: 0,
    view: 'list',
    addresses: [],
    pagination: {},
    address: {},
};
const reducers = {
    reset: state => {
        state.primary = 0;
        state = { ...initialState };
    },
    setView: (state, action) => {
        state.view = action.payload;
        state.address = {};
    },
    edit: (state, action) => {
        state.address = action.payload;
        state.view = 'form';
    },
};
const AddressSlice = createSlice({
    name: 'Address',
    initialState: initialState,
    reducers: reducers,
    extraReducers: {
        [store.pending]: () => {},
        [store.fulfilled]: (state, action) => {
            const { address } = action.payload;
            state.addresses = [address, ...state.addresses];
            // state.pagination = action.payload.pagination;
            state.view = 'list';
        },
        [store.rejected]: () => {},

        [list.pending]: () => {},
        [list.fulfilled]: (state, action) => {
            state.primary = action.payload.primary;
            state.addresses = action.payload.addresses;
            state.pagination = action.payload.pagination;
        },
        [list.rejected]: () => {},

        [update.pending]: () => {},
        [update.fulfilled]: (state, action) => {
            const updatedAddress = action.payload.address;
            state.addresses.map((address, key) => {
                if (address.id === updatedAddress.id) {
                    state.addresses[key] = updatedAddress;
                }
            });
            state.view = 'list';
        },
        [update.rejected]: () => {},

        [remove.pending]: () => {},
        [remove.fulfilled]: (state, action) => {
            const index = action.payload;
            const addresses = state.addresses.filter((address, key) => {
                if (index !== key) {
                    return address;
                }
            });
            state.addresses = addresses;
        },
        [remove.rejected]: () => {},

        [remove.pending]: () => {},
        [remove.fulfilled]: (state, action) => {
            const index = action.payload;
            const addresses = state.addresses.filter((address, key) => {
                if (index !== key) {
                    return address;
                }
            });
            state.addresses = addresses;
        },
        [remove.rejected]: () => {},

        [setPrimaryAddress.pending]: () => {},
        [setPrimaryAddress.fulfilled]: (state, action) => {
            state.primary = action.payload;
        },
        [setPrimaryAddress.rejected]: () => {},
    },
});
export const AddressReducer = AddressSlice.reducer;
export const AddressActions = AddressSlice.actions;
export const AddressStore = configureStore({
    reducer: { Address: AddressReducer },
    middleware,
});
