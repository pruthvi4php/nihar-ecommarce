import BaseRepository from '../../app/BaseRepository';
class AddressRepository extends BaseRepository {
    constructor() {
        super();
    }
    get = async () => this.api.post('address/get');
    store = async address => this.api.post('address/store', address);
    update = async address => this.api.post('address/update', address);
    remove = async id => this.api.post('address/remove', { id });
    list = async () => {
        const response = await this.api.post('address/list');
        return this.getDataWithPagination(response.data);
    };
    setPrimaryAddress = async id =>
        this.api.post('address/set-primary', { id });
}
export default AddressRepository;
