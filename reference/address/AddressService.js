import { Auth } from '../../../lib';
import AddressRepository from './AddressRepository';
//const _ = require('loadsh');
class AddressService extends Auth {
    constructor() {
        super();
        this.address = new AddressRepository();
    }

    store = async values => {
        const response = await this.address.store(values);
        if (response && response.data) {
            return { address: response.data };
        }
        return false;
    };
    /**
     * git change
     */
    list = async () => {
        const response = await this.address.list();
        if (response && response.data) {
            let { primary } = response.pagination;
            let address = response.data.find(a => a.id === parseInt(primary));
            return {
                primary: parseInt(primary),
                address,
                addresses: response.data,
                pagination: response.pagination,
            };
        }
        return false;
    };

    remove = async address => {
        const response = await this.address.remove(address.id);
        if (response) {
            return address.index;
        }
        return false;
    };

    update = async values => {
        const response = await this.address.update(values);
        if (response && response.data) {
            return { address: values };
        }
        return false;
    };

    setPrimaryAddress = async id => {
        const response = await this.address.setPrimaryAddress(id);
        if (response) {
            return id;
        }
        return false;
    };
}

export default AddressService;
