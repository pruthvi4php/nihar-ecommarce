import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { AddressStore } from './AddressStore';
//import AddressForm from './components/AddressForm';
import AddressList from './components/AddressList';
import AddressForm from './components/AddressForm';
const propTypes = {
    view: PropTypes.string,
};
class Address extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        require('./components/Address.scss');
    }
    render() {
        return (
            <Provider store={AddressStore}>
                <AddressList />
                <AddressForm />
            </Provider>
        );
    }
}
Address.propTypes = propTypes;
export default Address;
