import * as Yup from 'yup';
import { required } from '../../app/AppSchema';
const v = {
    required,
};

export const formValidation = (address = null) => {
    let initialValues = {
        id: '',
        address_name: '',
        address_line_1: '',
        address_line_2: '',
        postal_code: '',
    };

    if (address) {
        const {
            id,
            address_name,
            address_line_1,
            address_line_2,
            postal_code,
        } = address;
        initialValues = {
            id,
            address_name,
            address_line_1,
            address_line_2,
            postal_code,
        };
    }

    const formSchema = {
        address_name: v.required,
        address_line_1: v.required,
        address_line_2: v.required,
        postal_code: v.required,
    };
    return {
        initialValues,
        validations: Yup.object().shape(formSchema),
    };
};
