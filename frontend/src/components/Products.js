import styled from "styled-components";
import ProductItems from "./ProductItems";
import { products } from "../data";
import { mobile } from "../responsive";
import { useState, useEffect } from "react";
import axios from "axios";

const Container = styled.div`
  display: flex;
  padding: 20px;
  justify-content: space-between;
  flex-wrap: wrap;
  ${mobile({
    padding: "0px",
  })}
`;
const Products = ({ category, filters, sorts }) => {
  console.log("category: ", category);
  console.log("filters: ", filters);
  console.log("sorts: ", sorts);

  const [popularProducts, setPopularProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get(
          category
            ? `http://localhost:8000/api/products?categories=${category}`
            : "http://localhost:8000/api/products"
        );
        //console.log(res.data);
        setPopularProducts(res.data);
      } catch (error) {}
    };
    getProducts();
  }, [category]);

  useEffect(() => {
    category &&
      setFilteredProducts(
        popularProducts.filter((item) =>
          Object.entries(filters).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      );
  }, [category, popularProducts, filters]);

  useEffect(() => {
    if (sorts === "newest") {
      setFilteredProducts((popularProducts) =>
        [...popularProducts].sort((a, b) => a.createdAt - b.createdAt)
      );
    } else if (sorts === "asc") {
      setFilteredProducts((popularProducts) =>
        [...popularProducts].sort((a, b) => a.price - b.price)
      );
    } else {
      setFilteredProducts((popularProducts) =>
        [...popularProducts].sort((a, b) => b.price - a.price)
      );
    }
  }, [sorts]);

  return (
    <Container>
      {filteredProducts.map((item) => (
        <ProductItems key={item.id} image={item.image} />
      ))}
    </Container>
  );
};

export default Products;
